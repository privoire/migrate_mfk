/*-----------------------------------------------------------
  VERSION CONTROL BUILD SYSTEM                               
  This header file was created by VERBUILD v1.0.1            
  -----------------------------------------------------------
  help : verbuild -?                                         
  info : http://www.yeamaec.com                              
         yeamaec@hanafos.com ,krkim@yeamaec.com              
-----------------------------------------------------------*/

#ifndef VERSIONNO__H
#define VERSIONNO__H

#define VERSION_FULL           1.0.0.17

#define VERSION_BASEYEAR       0
#define VERSION_DATE           "2018-09-19"
#define VERSION_TIME           "15:13:40"

#define VERSION_MAJOR          1
#define VERSION_MINOR          0
#define VERSION_BUILDNO        0
#define VERSION_EXTEND         17

#define VERSION_FILE           1,0,0,17
#define VERSION_PRODUCT        1,0,0,17
#define VERSION_FILESTR        "1,0,0,17"
#define VERSION_PRODUCTSTR     "1,0,0,17"

#endif
