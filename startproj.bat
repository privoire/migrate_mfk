echo off
cls
REM Here is project folder look like for use this batch file
REM root/git
REM |- teamcenter-tools
REM ||- incl
REM ||- src
REM |- percall-handlers-library
REM ||- BMIDE
REM ||- DELIVERY
REM ||- DEVT
REM |||- PERCALL_HANDLERS_LIBRARY
REM ||||- ITK
REM |||||- handlers
REM ||||||- this.bat
REM ||- SCRIPT

:SCRIPT_START

set TC_ROOT=C:\Siemens\TCROOT_DEV_INCLUDES
set TC_DATA=C:\Siemens\TC11_DATA
set MSVCDir=C:\Program Files (x86)\Microsoft Visual Studio 11.0
set PROJECT_ROOT=%~dp0
set PERCALL_TOOL_DEV=D:\GIT\teamcenter-tools

echo TC_ROOT     = "%TC_ROOT%"
echo TC_DATA     = "%TC_DATA%"
echo MSVCDir     = "%MSVCDir%"
echo PROJECT_ROOT= "%PROJECT_ROOT%"
echo PERCALL_TOOL_DEV = "%PERCALL_TOOL_DEV%"
set TC_INCLUDE=%TC_ROOT%\include
set TC_LIBRARY=%TC_ROOT%\lib

echo ------------------------------
echo Checkin variables integrity...
for %%a in ("%TC_ROOT%" "%MSVCDir%" "%PROJECT_ROOT%" "%PERCALL_TOOL_DEV%" "%TC_INCLUDE%" "%TC_LIBRARY%") do (
                IF not exist "%%a\" (
                               echo -----------
                               echo Variable with wrong path
                               echo Aborting
                               goto :END_SCRIPT
                ) else IF %%a=="" (
                               echo -----------
                               echo Variable empty
                               echo Aborting
                               goto :END_SCRIPT
                ) else (
                               echo %%a checked
                )
)
goto :START_MSVC

:START_MSVC
echo. #======================================
echo. TC_ROOT=%TC_ROOT%
echo. TC_DATA=%TC_DATA%
echo. PROJECT_ROOT=%PROJECT_ROOT%
echo. PERCALL_TOOL_DEV=%PERCALL_TOOL_DEV%
echo. #======================================

start "%MSVCDir%\Common7\IDE\devenv.exe" "%PROJECT_ROOT%\percall_loader.sln"

:END_SCRIPT

