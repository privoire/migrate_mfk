#include <iostream>
#include <string>
#include <chrono>
#include <ctime>
#include "itk/ITK.h"
#include "tc/PTcSyslog.hpp"
#include "tc/PTcSite.hpp"
#include "tc/PTcEncryption.hpp"
#include "tc/PTcPref.hpp"
#include "tc/PTcItem.hpp"
#include "tc/PTcDataset.hpp"
#include "tc/PTcPrimitiveType.hpp"
#include "util/FileUtils.hpp"


#include "VersionNo.h"

#define NB_VALUE_DATE 6
#define NB_VALUE_NEED 21
#define NB_VALUE_OPTI_NEED 6
#define PERCALL_LOADER 0

#define DATE_KEY_NAME "Date"
#define NEED_KEY_NAME "Check"

using namespace std;

// Exemple de manipulation des API
int ITK_user_main(int argc,char *argv[])
{
	PTcSyslog* 	syslog;		/*!< Syslog object for Trace */
	string spdctver0 = VERSION_PRODUCTSTR;
	string spdctver;
	spdctver = Utils::replaceAll (spdctver0, ",", ".");
	cout << endl << " <<<< Starting migrate_mfk version " << spdctver.c_str()<< " from "<< VERSION_DATE<< " " <<VERSION_TIME<< " >>>> " <<endl;
	
	auto start = chrono::system_clock::now();
	time_t start_time = chrono::system_clock::to_time_t(start);
	cout << "Start date : " << ctime(&start_time) << endl;

	try {
		//Récupération des Arguments
		char*      cliHelp    	= ITK_ask_cli_argument("-h");
		char*      cliUser    	= ITK_ask_cli_argument("-u=");
		char*      cliPassw   	= ITK_ask_cli_argument("-p=");
		char*      cliPasswfile = ITK_ask_cli_argument("-pf=");
		char*      cliGrp     	= ITK_ask_cli_argument("-g=");
		char*      cliLog    	= ITK_ask_cli_argument("-log=");
		char*      cliInFile    = ITK_ask_cli_argument("-input=");
		char*      cliOutFile   = ITK_ask_cli_argument("-out=");
				
		//Analyse des Arguments
		bool	bargerr = false;
		int		ninput = 0;

		// Fichier log
		if (cliLog != NULL) syslog = PTcSyslog::instance(strdup(cliLog), true);else syslog = PTcSyslog::instance();
		if (cliHelp== NULL)
		{
			if (cliPasswfile == NULL || strcmp (cliPasswfile, "")== 0)
			{
				if (cliUser == NULL || strcmp (cliUser, "")== 0) 
				{
					cout << "Error : missing mandatory parameter -u" << endl;
					bargerr=true;
				}
				if (cliPassw == NULL || strcmp (cliPassw, "")== 0) 
				{
					cout << "Error : missing mandatory parameter -p" << endl;
					bargerr=true;
				}
				if (cliGrp == NULL || strcmp (cliGrp, "")== 0) 
				{
					cout << "Error : missing mandatory parameter -g" << endl;
					bargerr=true;
				}
			}
	

			if (cliInFile == NULL || strcmp (cliInFile, "")== 0) 
			{
				cout << "Error : missing mandatory parameter -input" << endl;
				bargerr=true;
			}
			if (cliOutFile == NULL || strcmp (cliOutFile, "")== 0) 
			{
				cout << "Error : missing mandatory parameter -out" << endl;
				bargerr=true;
			}
		}
	

		if (bargerr || cliHelp != NULL) 
		{
			cout << " Syntax :" << endl;
			cout << " migrate_mfk [-u=... -p=... -g=...| -pf=....] ";
			cout << "         -input=<input file pathname> -out=<output file pathname> -log=<log file pathname>"  << endl;
			cout << " " << endl;
			cout << " -u : User name" << endl;
			cout << " -p : Password" << endl;
			cout << " -g : Group name" << endl;
			cout << " -out : Pathname of output file" << endl;
			cout << " -log : Pathname of log file" << endl;
			cout << " –input : Pathname of input file" << endl;
			cout << " " << endl;
		}
		else {
			syslog->message(0, "Launching migrate_mfk %s  %s %s", spdctver.c_str(), VERSION_DATE, VERSION_TIME );
			syslog->message(0, "==> Parameters : ");
			syslog->message(0, "	==> u : [%s]" , cliUser);
			syslog->message(0, "	==> p : [%s]", cliPassw);
			syslog->message(0, "	==> g : [%s]", cliGrp);
			syslog->message(0, "	==> pf : [%s]", cliPasswfile);
			syslog->message(0, "	==> input : [%s]", cliInFile);
			syslog->message(0, "	==> out : [%s]", cliOutFile);
			syslog->message(0, "	==> log : [%s]", cliLog);
			
	
			int        retcode    = ITK_ok;
			bool       ctrlpass   = false;
			/* connection to TC */		
			if (cliPassw == NULL)
			{
				syslog->message(0, " Auto login");
				retcode = ITK_auto_login();
			}
			else
			{
				syslog->message(0, " Connecting to TC :[%s],[%s],[%s]", cliUser,cliPassw, cliGrp);
				retcode = ITK_init_module(cliUser, cliPassw, cliGrp);
			}
			syslog->message(0, " Connecting to TC :[%d]", retcode);
			if (retcode != ITK_ok) return retcode;
			ITK_set_bypass(true);
	
		

			try {
				bool _mberror = false;
				ofstream		_ficout;
				fstream			_ficinput;
				string	slig;

				syslog->message(0, "		==> Initialization ");
				// Ouverture du fichier de config
				if (!FileUtils::fileExists(cliInFile))
				{	
					cout << "Unknown input file :" << cliInFile << endl;
					_mberror=true;
				}
				_ficinput.open(cliInFile,ios::in);

				syslog->message(0, "		==> Opening output file ");
				_ficout.open(cliOutFile, ios::out); //on ouvrre le fichier en ecriture 
				if (_ficout.bad())
				{
					//cout << "Erreur : Impossible de créer le Fichier de sortie "<< endl;
					syslog->message(LOG_ERROR, "Error : Cannot create output file ");
					_mberror=true;
				}
				if (!_mberror) 
				{
					int nlig, ni = 0;

					while( !_ficinput.eof() )
						{
							string sout;
							nlig++;
							ni = nlig/100;
							if (ni*100 == nlig) cout << "Processing line " << nlig << endl;
				
							getline(_ficinput, slig);//lecture d'une ligne du fichier
							if (slig.compare ("") != 0) 
							{
								sout = slig+";";
								try {
									syslog->message (LOG_ERROR, "Traitement ItemId : %s", slig.c_str());
									vector<string> datas;
									Utils::splitString (slig, datas, "-", false);
									if (datas.size() != 2)
									{
										syslog->message (LOG_ERROR, "==> ERROR format de l'item_id erroné %s", slig.c_str());
										sout+="ERROR\n";
										_ficout.write (sout.c_str(), sout.size());
										continue;
									}
									PTcItem pitm = PTcItem::findItem (slig);
									if (pitm.isValidTag())
									{
										pitm.refresh (POM_modify_lock);
										pitm.setInternalAttrValue ("item_id", datas[0]);
										pitm.setInternalAttrValue ("as4_SheetNumberName", datas[1]);
										pitm.save ();

										// Recherche des Dataset attachés
										PTcItemRevision pitmrev = pitm.getLatestRevision();
										list<PTcWorkspaceObject> lds = pitmrev.getSecondaryObjects ("IMAN_specification");
										for (list<PTcWorkspaceObject>::iterator it=lds.begin(); it != lds.end(); it++)
										{
											if (it->isDescendant2 ("Dataset"))
											{
												syslog->message (LOG_ERROR, "==> Traitement du fichier %d  %s", it->getTag(), it->getString().c_str());
												PTcDataset pds;
												pds.setTag (it->getTag());
												pds.refresh (POM_modify_lock);
												pds.load();
												
												PTcImanFile pfile = pds.findNamedRef(0);
												if (pfile.isValidTag())
												{
													string sname;
													syslog->message (LOG_ERROR, "	==> Traitement de la référence nommée %s %s", pds.getDatasetType ().c_str(), pfile.askOriginalFileName().c_str());
													if (pds.getDatasetType ().compare ("CATDrawing")==0) sname = datas[0]+"_"+datas[1]+".CATDrawing"; 
														else if (pds.getDatasetType ().compare ("DrawingSheet")==0) sname = datas[0]+"_"+datas[1]+".cgm";
													syslog->message (LOG_ERROR, "	   ==> Nouveau nom : %s", sname.c_str());
													pfile.refresh (POM_modify_lock);
													pfile.load();
													pfile.setOriginalFileName (sname);
													pfile.save ();
												}
												string sname = datas[0]+"-"+datas[1]+"/"+pitmrev.getInternalAttrValue("item_revision_id");
												pds.setName (sname);
												//pds.setInternalAttrValue (
												pds.save ();

											}
										}
										sout+="OK\n";
										_ficout.write (sout.c_str(), sout.size());
										
									}
									else {
										syslog->message (LOG_ERROR, "==> ERROR ItemId : %s inconnu", slig.c_str());
										sout+="ERROR\n";
										_ficout.write (sout.c_str(), sout.size());
										
									}




								}
								catch (const PTcException& e) {
									string errMessage = Utils::objToString(e.what())
											+"\n\t" + "Exception met on input line number " + to_string(nlig);
									cout << errMessage<<endl;
									syslog->message(LOG_ERROR, errMessage);
								}
							}
						}
				}
					
			}
			catch (const PTcException& e) {
				cout << "Exception met while running an Import" << endl;
				cout << e.what() << endl;
			}
	
		}
	}
	catch (const PTcException& e) {
		cout << "PTcException met while running the Loader, terminating..." << endl;
		cout << e.what() << endl;
	}


	cout << endl << " <<<< Ending migrate_mfk >>>> " << endl;
	
	auto end = chrono::system_clock::now();
	time_t end_time = chrono::system_clock::to_time_t(end);
	cout << "End date : " << ctime(&end_time) << endl;
	
	//Calculating execution time
	chrono::duration<double> elapsed_seconds = end-start;
	double nbSeconds = elapsed_seconds.count();
	double nbMinutes = nbSeconds / 60;
	double nbHours = nbMinutes / 60;
	cout << "Total run time : ";
	if (nbHours >= 1) {
		cout << (int)nbHours <<"h ";
		nbSeconds = nbSeconds - (int)nbHours*3600;
		nbMinutes = nbMinutes - (int)nbHours*60;
	}
	if (nbMinutes >= 1) {
		cout << (int)nbMinutes <<"m ";
		nbSeconds = nbSeconds - (int)nbMinutes*60;
	}
	cout << nbSeconds << "s" << endl;
	
	return 0;
}
